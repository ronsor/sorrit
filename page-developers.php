<h2>Development</h2>
Sorrit's design makes it easy to create bots and access our content.
<br/>
<b>Table of contents</b>
<ul>
<li>Fetch a post and all its comments</li>
<li>Check if a post exists</li>
<li>Authentication</li>
<li>Create a post</li>
</ul>
<hr/>
<h3>1. Fetch a post and all its comments</li>
<pre>
Make a HTTP Request to: http://sorrit.ml/posty/POST_TITLE.SUBSORRIT.txt
Method: GET
Response: 200 OK: database file containing all post content.
Response Format:
TITLE(can be blank)
AUTHOR
URL(can be blank)
CONTENT(with<br/> and htmlentities)
(blank line)
TIMESTAMP(or 0 for exported-from-reddit posts)
0|0

==
------------------ End response.
Entries are repeated.

The POST_TITLE should be urlencoded with spaces being a '+' plus sign, not %20!
</pre>
<h3>Check if a post exists</h3>
<pre>
Repeat what you did with '1' but check for a 404 error. If you get one, the post doesn't exist.
You cannot check if comments exist without doing '1' and searching the (could be large?) file.
</pre>
<h3>Authentication</h3>
<pre>
SEND A HTTP POST REQUEST TO: http://sorrit.ml/?login.
Parameters: user=username, pass=password
If you have logged in with success, you should get the text 'Logged in.' or 'User Panel' somewhere on the page.
Response: HTML.
</pre>

